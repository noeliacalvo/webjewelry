<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="StyleSheet" href="formatform.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" 
    integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" 
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" 
    integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" 
    integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
    <script src="sweetalert2.min.js"></script>
    <link rel="stylesheet" href="sweetalert2.min.css">
</head>

<body>
    <header>
        <h2 class="logo">Corporative</h2>
        <input type="checkbox" id="check">
        <label for="check" class="mostrar-menu">
            &#8801
        </label>

        <nav class="menu">
            <a href="index.php">Inicio</a>
            <a href="buildarticles.php">Artículos</a>
            <a href="buildform.php">Contacto</a>
            <a href="buildinfo.php">sobre nosotros</a>
            <label for="check" class="esconder-menu">
                &#215
            </label>
        </nav>
        <nav class="menu-iconos">
            <div id="icon-search">
                <i class="fas fa-search" id="icon-search"></i>
            </div>
            <div id="icono-usuario">
                <a href="usuario.php"><i class="far fa-user"></i></a>
            </div>
            <div id="icono-megusta">
                <a href="heart.php"><i class="fas fa-heart"></i></a>
            </div>
            <div id="icono-compra">
                <a href="shop.php"><i class="fas fa-shopping-cart"></i></a>
            </div>
        </nav>
    </header>

    <section id="contacto">
        <h4>Contacta con nosotros</h4>
        <form action="conexion/contactaconnosotros.php" method="POST">
            <div class="datos-form">
                <div class="nombre">
                    <label for="nombre">Nombre</label>
                    <input type="text" name="nombre" id="nombre" placeholder="Nombre">
                </div>
                <div class="email">
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" placeholder="Email">
                </div>
            </div>
            <div class="mensaje">
                <label for="mensaje">Mensaje</label>
                <textarea name="mensaje" id="mensaje" cols="30" row="10" placeholder="Mensaje"></textarea>
            </div>
            <div class="boton-formulario">
                <button type="submit" class="boton-saber-mas">Enviar Mensaje.</button>
            </div>
        </form>
    </section>




        <section id="icons">
            <div class="contenido-iconos">
                <div>
                    <i class="fas fa-shipping-fast fa-3x"></i>
                    <h6>Envios rápidos.</h6>
                </div>
                <div>
                    <i class="fas fa-unlock-alt fa-3x"></i>
                    <h6>Pago seguro.</h6>
                </div>
                <div id="icono-tienda">
                    <i class="fas fa-store-alt fa-3x"></i>
                    <h6>Tiendas.</h6>
                </div>
            </div>
        </section>
        <footer>
            <p>
                &copy; 2021 Naviscode
            </p>
        </footer>


        <script src="Js\menudesplegable.js"></script>
        <script src="Js\cartshop.js"></script>
        <script src="Js\pedido.js"></script>
    </body>

</html>