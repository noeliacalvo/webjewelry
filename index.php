<!doctype html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="StyleSheet" href="format.css">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" 
    integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" 
    integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" 
    integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" 
    integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" 
    integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/sweetalert2.min.css">
    <script src="js/sweetalert2.min.js"></script>
    
    
</head>

<body>
    <header>
    
        <div class="header-content">
            <h2 class="logo">Jewelry</h2>
            <input type="checkbox" id="check">
            <label for="check" class="mostrar-menu">
                &#8801
            </label>
            <nav class="menu">
                <a href="index.php">Inicio</a>
                <a href="buildarticles.php">Artículos</a>
                <a href="buildform.php">Contacto</a>
                <a href="buildinfo.php">Conócenos</a>
                <label for="check" class="esconder-menu">
                    &#215
                </label>
            </nav>
    
            <nav class="menu-iconos navbar navbar-expand-md navbar-dark fixed">
                <div id="icon-search">
                    <i class="fas fa-search" id="icon-search"></i>
                </div>
                <div id="icono-usuario">
                    <a href="usuario.php"><i class="far fa-user"></i></a>
                </div>
                <div id="icono-megusta">
                    <a href="admin.php"><i class="fas fa-heart"></i></a>
                </div>
                <div id="icono-compra">
                    <a href="shop.php"><i class="fas fa-shopping-cart"></i></a>
                </div>
                

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                        <img src="IMG/carro.png" class="nav-link dropdown-toggle img-fluid"
                        height="150px" width="150px" color="white" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"></img>
                        <div id="carrito" class="dropdown-menu" aria-labelledby="navbarCollapse">
                            <table id="lista-carrito" class="table">
                                <thead>
                                    <tr>
                                        <th>Imagen</th>
                                        <th>Nombre</th>
                                        <th>Precio</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
        
                            <a href="#" id="vaciar-carrito" class="btn btn-primary btn-block">Vaciar Carrito</a>
                            <a href="#" id="procesar-pedido" class="btn btn-danger btn-block">Procesar
                                Compra</a>
                        </div>
                    </li>
                </ul>
            </div>

            </nav>




    
            <div id="ctn-bars-search">
                <input type="text" id="inputSearch" placeholder="¿Qué deseas buscar?">
            </div>
            <ul id="box-search">
                <li><a href="#"><i class="fas fa-search"></i> Pendientes</a></li>
                <li><a href="#"><i class="fas fa-search"></i> Pendiente</a></li>
                <li><a href="#"><i class="fas fa-search"></i> Pendientes largos</a></li>
                <li><a href="#"><i class="fas fa-search"></i> Collar</a></li>
                <li><a href="#"><i class="fas fa-search"></i> Collares</a></li>
                <li><a href="#"><i class="fas fa-search"></i> Pulsera</a></li>
                <li><a href="#"><i class="fas fa-search"></i> Pulseras</a></li>
            </ul>
    
            <div id="cover-ctn-search"></div>
        </div>
    </header>



    <section id="banner">
       
        <div class="contenido-banner">
            <h3><span>CORPOTATIVE:</span>WEB RESPONSIVE <br>MOBILE FRIENDLY</h3>
            <a href="#" class="boton-empezar">Descubre la última collectión </a>

        </div>
    </section>

    <section id="masvendidos">
        
        <div>
            <img src="IMG\pndcirculo5.jpg" alt="">
            <h4>Noche estrellada</h4>
            <p>Te hará recordar las noches de verano viendo las estrellas</p>
            <h6 class="precio"><span class="">39€</span></h6>
            <a href="" class="boton-comprar" data-id="1">Comprar</a>
            <a href="" class="boton-like"><i class="far fa-heart"></i></a>
        </div>
        <div>
            <img src="IMG\pndor1.jpg" alt="">
            <h4>Desierto</h4>
            <p>Pendiente de oro con brillantes.</p>
            <h6 class="precio"><span class="">39€</span></h6>
            <a href="" class="boton-comprar" data-id="2">Comprar</a>
            <a href="" class="boton-like"><i class="far fa-heart"></i></a>
        </div>
        <div>
            <img src="IMG/pndor4.jpg" alt="">
            <h4>Luna</h4>
            <p>Para dar un toque especial a todos los outfits.</p>
            <h6 class="precio"><span class="">39€</span></h6>
            <a href="" class="boton-comprar" data-id="3">Comprar</a>
            <a href="" class="boton-like"><i class="far fa-heart"></i></a>
        </div>
        <div>
            <img src="IMG/pndor3.jpg" alt="">
            <h4>Diamante con oro </h4>
            <p>Para ocasiones especiales resaltar y alumbrar.</p>
            <h6 class="precio"><span class="">39€</span></h6>
            <a href="" class="boton-comprar" data-id="4">Comprar</a>
            <a href="" class="boton-like"><i class="far fa-heart"></i></a>
    </div>
    </section>
   

<section id="icons">
    <div class="contenido-iconos">
        <div>
            <i class="fas fa-shipping-fast fa-3x"></i>
            <h6>Envios rápidos.</h6>
        </div>
        <div>
            <i class="fas fa-unlock-alt fa-3x"></i>
            <h6>Pago seguro.</h6>
        </div>
        <div id="icono-tienda">
            <i class="fas fa-store-alt fa-3x"></i>
            <h6>Tiendas.</h6>
        </div>


    </div>
</section>
<footer>
    <p>
        &copy; 2021 Naviscode
    </p>
</footer>


<script src="Js\menudesplegable.js"></script>
<script src="Js\cartshop.js"></script>
<script src="Js\pedido.js"></script>



</body>



</html>