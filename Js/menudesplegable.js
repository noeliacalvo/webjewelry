

                                                        //Buscador de contenido


///Ejecutando funciones

document.getElementById("icon-search").addEventListener("click", mostrar_buscardor);
document.getElementById("ctn-bars-search").addEventListener("click", ocultar_buscador);



//Declaración de variables
bars_search        =    document.getElementById("ctn-bars-search");
cover_ctn_search   =    document.getElementById("cover-ctn-search");
inputSearch        =    document.getElementById("inputSearch");
box_search         =    document.getElementById("box-search");

//Funcion para mostrar el buscador

function mostrar_buscardor(){

    bars_search.style.top = "80px";
    cover_ctn_search.style.display="block";
    bars_search.style.display="block";
    inputSearch.focus();
    box_search.style.display = "none";


    if (inputSearch.value === ""){
        box_search.style.display = "none";
    }

}

/*Funcion para ocultar el buscardor */

function ocultar_buscador(){

        bars_search.style.top = "-10px";
        cover_ctn_search.style.display = "none";
        bars_search.style.display="none";
        inputSearch.value= "";
}


//Creando filtrado de busqueda

document.getElementById("inputSearch").addEventListener("keyup", buscardor_interno);

function buscardor_interno(){

    filter =  inputSearch.value.toUpperCase();
    li     =  box_search.getElementsByTagName("li");


        //Recorriendo elementos a filtrar mediantes los li

    for( i = 0; i < li.length ; i++){

        a = li[i].getElementsByTagName("a")[0];
        textValue = a.textContent || a.innerText;

        if(textValue.toUpperCase().indexOf(filter) > -1 ){

            li[i].style.display = " ";
            box_search.style.display = "block";

            if (inputSearch.value === ""){
                box_search.style.display = "none";
            }

        }else{

            li[i].style.display = "none";
        }


    }


    }