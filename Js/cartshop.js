

//function add product shopping cart

class Carrito{

    comprarProducto(e){
        e.preventDefault();
        if(e.target.classList.contains("boton-comprar")){
        const producto = e.target.parentElement.parentElement;
        this.leerDatosProducto(producto);

        }
    }

    leerDatosProducto(producto){
        const infoProducto = {
            imagen : producto.querySelector('img').src,
            titulo : producto.querySelector('h4').textContent,
            precio : producto.querySelector('.precio span').textContent,
            id : producto.querySelector('a').getAttribute('data-id'), //poner un ID para borrar luego un producto especifico
            cantidad : 1
        }

        this.insertarCarrito(infoProducto);
        this.mostrarProducto();
    }

    insertarCarrito(producto){
        const row = document.createElement('tr');
        row.innerHTML =  `
        <td> 
            <img src="${producto.imagen}" width=100> 
        </td>   
        <td>${producto.titulo}</td>   
        <td>${producto.precio}</td>    
        <td>
            <a href=# class="borrar-producto fas fa-times-circle" data-id="${producto.id}"></a> 
        </td>
        `;
        console.log(row);
        listaProductos.appendChild(row);
        this.mostrarProducto();
    }

    mostrarProducto(){
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Se ha agregado el artículo a tu pedido.',
            showConfirmButton: false,
            timer: 1500
        })
    }

                                           //Eliminar Productos

    eliminarProducto(e){
        e.preventDefault();
        let producto, productoID;
        if(e.target.classList.contains('borrar-producto')){
            e.target.parentElement.parentElement.remove();
            producto = e.target.parentElement.parentElement;
            productoID = producto.querySelector("a").getAttribute('data-id');

        }

    }

    vaciarCarrito(e){
        e.preventDefault();
        while(listaProductos.firstChild){
            listaProductos.removeChild(listaProductos.firstChild);
        }
        return false;
    }

    mostrarLike(){
        Swal.fire({
            type:'success',
            type: 'mensaje',
            text: 'Articulo guardo en me gusta.'
        });
    }

} 
